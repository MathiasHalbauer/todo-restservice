FROM openjdk:17-jdk-alpine
MAINTAINER mathias.halbauer
COPY target/my-spring-boot-project-0.0.1-SNAPSHOT.jar my-spring-boot-project-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/my-spring-boot-project-0.0.1-SNAPSHOT.jar"]
