package com.example.todorestservice;

import com.example.todorestservice.entity.ToDo;
import com.example.todorestservice.repository.ToDoRepository;
import com.example.todorestservice.service.ToDoService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class ToDoServiceTest {

    private final ToDoRepository repo = mock(ToDoRepository.class);

    private ToDoService toDoService = new ToDoService(repo);
    private ToDoService spiedToDoService = spy(toDoService);

    String malformatted = "Malformatted UUID.";
    String nonexistent = "No element to retrieve.";
    UUID uuid1 = UUID.randomUUID();
    UUID uuid2 = UUID.randomUUID();
    UUID uuid3 = UUID.randomUUID();
    UUID uuid4 = UUID.randomUUID();
    UUID uuid5 = UUID.randomUUID();
    UUID uuid6 = UUID.randomUUID();

    ToDo todo1 = new ToDo(uuid1, "todo1", "The first todo", new ArrayList<>());
    ToDo todo2 = new ToDo(uuid2, "todo2", "The second todo", new ArrayList<>());
    ToDo todo3 = new ToDo(uuid3, "todo3", "The third todo", new ArrayList<>());
    ToDo todo6 = new ToDo(uuid6, "todo6", "the sixth todo", new ArrayList<>());


    @BeforeEach
    void setUp(){
        doReturn(uuid1).when(spiedToDoService).convertStringToUUID("01");
        doReturn(uuid2).when(spiedToDoService).convertStringToUUID("02");
        doReturn(uuid3).when(spiedToDoService).convertStringToUUID("03");
        doReturn(uuid4).when(spiedToDoService).convertStringToUUID(nonexistent);

        doReturn(uuid6).when(spiedToDoService).convertStringToUUID("06"); //umgeht die Abfrage des Objekts

        List<ToDo> todos = new ArrayList<>();
        todos.add(todo1);
        todos.add(todo2);

        doReturn(todos).when(repo).findAll();
        doReturn(Optional.of(todo1)).when(repo).findById(uuid1);
        doReturn(Optional.of(todo2)).when(repo).findById(uuid2);
        doReturn(Optional.of(todo3)).when(repo).findById(uuid3);
        doReturn(Optional.of(todo6)).when(repo).findById(uuid6); //problematisch im Test
        //

        doAnswer(invocation -> {
            todos.remove(todo2);
            return null;
        }).when(repo).deleteById(uuid2);
    }

    @Test
    void testGetAllToDos() {
        List<ToDo> allToDos = toDoService.getAllToDos();

        assertEquals(allToDos.size(), 2);
        assertEquals(allToDos.get(0), todo1);
        assertEquals(allToDos.get(1), todo2);
        assertFalse(allToDos.contains(todo3));
    }

    @Test
    void testGetSpecificToDo() {
        assertEquals(spiedToDoService.getSpecificToDo("01"), todo1);
        assertEquals(spiedToDoService.getSpecificToDo("02"), todo2);
        assertEquals(spiedToDoService.getSpecificToDo(nonexistent).getName(), nonexistent);
        assertEquals(spiedToDoService.getSpecificToDo(malformatted).getName(), malformatted);
    }

    @Test
    void testAddToDo() {
        spiedToDoService.addToDo(todo3.getName(), todo3.getDescription(), todo3.getTasks());

        assertEquals(spiedToDoService.getSpecificToDo("03").getName(), todo3.getName());
        assertEquals(spiedToDoService.getSpecificToDo("03").getDescription(), todo3.getDescription());
        assertEquals(spiedToDoService.getSpecificToDo("03").getTasks(), todo3.getTasks());
    }

    @Test
    void testUpdateToDo(){
        //set up
        //doReturn(todo6).when(spiedToDoService).addToDo(todo6.getName(),todo6.getDescription(), new ArrayList<>());
        ToDo updated = new ToDo(uuid6, "todo6Updated","ToDo was updated", new ArrayList<>() );

        //actual method call
        toDoService.updateToDo(uuid6.toString(), updated);

        //when(repo.findById(uuid6)).thenCallRealMethod(); // funktioniert nicht, weil findById geeerbt wird
        //assert
        assertEquals(spiedToDoService.getSpecificToDo("06").getName(), updated.getName());
        //assertEquals(spiedToDoService.getSpecificToDo("06").getDescription(), updated.getDescription());
        //assertEquals(spiedToDoService.getSpecificToDo("06").getTasks(), todo3.getTasks());
    }

    @Test
    void testDeleteToDo() {
        spiedToDoService.deleteToDo("02");
        List<ToDo> allToDos = toDoService.getAllToDos();

        assertTrue(allToDos.contains(todo1));
        assertFalse(allToDos.contains(todo2));
        assertEquals(allToDos.size(), 1);
    }

    @Test
     void testConvertStringToUUIDValidID(){
        String id = "e6b3b375-8633-47cd-897a-cdef7f7bfdc7";
        var realUUID = UUID.fromString(id);
        var generatedUUID = toDoService.convertStringToUUID(id);

        assertEquals(generatedUUID, realUUID);


    }

}