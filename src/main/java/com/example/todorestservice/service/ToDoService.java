package com.example.todorestservice.service;

import com.example.todorestservice.entity.Task;
import com.example.todorestservice.entity.ToDo;
import com.example.todorestservice.repository.ToDoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import java.util.*;

@Service
public class ToDoService {
    private static final String MALFORMATTED_ID = "Malformatted UUID.";
    private static final String NO_ELEMENT_TO_UPDATE = "No element to update.";
    private static final String NO_ELEMENT_TO_RETRIEVE = "No element to retrieve.";
    private final ToDoRepository repo;
    @Autowired
    public ToDoService(ToDoRepository repo){
        this.repo = repo;
    }

    public List<ToDo> getAllToDos(){
        List<ToDo> toDoList = new ArrayList<ToDo>();
        toDoList = this.repo.findAll();
        return toDoList;
    }

    public ToDo addToDo(@NotBlank String name, String description, List<Task> tasks){

        UUID myUUId = UUID.randomUUID();

        if (description.isBlank()){
            description = "{"+  name +"} consists of "+ tasks.size() + "tasks";
        }

        ToDo myToDo = new ToDo(name, description, tasks);

        myToDo.setId(myUUId); //inserted to avoid having a constructor where the Id has to be given, because of problems with deserialization

        for (Task myTask: myToDo.getTasks()) {
            myTask.setId(myToDo.getId());
        }

        repo.save(myToDo);
        return myToDo;
    }

    public ToDo getSpecificToDo(String id){
        try {
            Optional<ToDo> toDo = repo.findById(convertStringToUUID(id));
            if (toDo.isPresent()){
                return toDo.get();
            } else {
                return new ToDo(NO_ELEMENT_TO_RETRIEVE);
            }
        } catch (IllegalArgumentException iae) {
            return new ToDo(MALFORMATTED_ID);
        }
    }

    public ToDo updateToDo(String id, ToDo newToDo){
        try {
            if (repo.existsById(convertStringToUUID(id))){
                newToDo.setId(convertStringToUUID(id));
                repo.deleteById(convertStringToUUID(id));
                repo.save(newToDo);
                return newToDo;
            }

            else return new ToDo(NO_ELEMENT_TO_UPDATE);

        } catch (IllegalArgumentException iae){
            return new ToDo(MALFORMATTED_ID);
        }
    }
    public void deleteToDo(@NotBlank String id){
        repo.deleteById(convertStringToUUID(id));
    }

    /*
    Helper-Function to handle necessary Type-Conversion
    @param
    id: String from URL, which signifies the id of the ToDo that should be queried
    return: UUID necessary to retrieve the queried ToDo-Instance
     */

    public UUID convertStringToUUID(String id){
        UUID myUUID = UUID.fromString(id);
        return myUUID;
    }

}
