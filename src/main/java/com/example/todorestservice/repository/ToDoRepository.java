package com.example.todorestservice.repository;

import com.example.todorestservice.entity.ToDo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("postgres")
public interface ToDoRepository extends JpaRepository<ToDo, java.util.UUID> {
}
