package com.example.todorestservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import org.springframework.lang.NonNull;
import javax.validation.constraints.NotBlank;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name="task")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID taskId;


    @NotBlank
    private String name;
    private String description;
    //@MapsId

    @ManyToOne()
    @JoinColumn(name="id", foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @JsonIgnore
    private ToDo todo;

    public Task(@NonNull String name, String description, ToDo myToDo) {
        this.name = name;
        this.description = description;
        this.taskId = myToDo.getId();
    }

    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                "id='" + taskId + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return taskId.equals(task.taskId) && name.equals(task.name) && description.equals(task.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskId, name, description);
    }

    public Task() {
    }

    public ToDo getTodo() {
        return todo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(UUID id){
        this.taskId = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTodo(ToDo todo) {
        this.todo = todo;
    }

    public String getDescription(){
        return this.description;
    }

    public String getName(){
        return this.name;
    }
}
