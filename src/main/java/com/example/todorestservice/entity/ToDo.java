package com.example.todorestservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import org.springframework.lang.NonNull;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name="todo")
public class ToDo {
    @Id
    private UUID id;
    @NotBlank
    private String name;

    private String description;

    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name="id", foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    private List<Task> tasks;

    public ToDo() {
    }

    public ToDo( @JsonProperty("name") @NonNull String name,  @JsonProperty("description") String description, @JsonProperty("tasks") List<Task> tasks) {
        this.name = name;
        this.description = description;
        this.tasks = tasks;
    }

    public ToDo(String name){
        this.name = name;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public UUID getId() {
        return id;
    }



    public String getName() {
        return name;
    }

    public List<Task> getTasks(){
        return this.tasks;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ToDo toDo = (ToDo) o;
        return id.equals(toDo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "ToDo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", nr of tasks="  + tasks.size() +
                '}';
    }


}
