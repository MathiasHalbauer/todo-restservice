package com.example.todorestservice.controller;

import com.example.todorestservice.entity.ToDo;
import com.example.todorestservice.service.ToDoService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ToDoController {
    private final ToDoService service;

    public ToDoController(ToDoService service){
        this.service = service;
    }

    @GetMapping("/todos")
    public List<ToDo> getAllToDos(){
        return service.getAllToDos();
    }

    @PostMapping("/todos")
    public ToDo addToDo(@RequestBody ToDo myToDo){
        return service.addToDo(myToDo.getName(), myToDo.getDescription(), myToDo.getTasks());
    }

    @GetMapping("/todos/{id}")
    public ToDo getToDo(@PathVariable("id") String id){
        return service.getSpecificToDo(id);
    }

    @PutMapping("/todos/{id}")
    public ToDo updateToDo(@PathVariable("id") String id, @RequestBody ToDo updatedToDo){
        return service.updateToDo(id, updatedToDo);
    }

    @DeleteMapping("/todos/{id}")
    public void deleteToDo(@PathVariable("id") String id){
        service.deleteToDo(id);
    }
}
