# ToDo-RestService

# Aim
Although the service is rather simple to aim of this repository is to implement setup for (almost) production-ready deployments and to learn adopting Gitlab CI/CD, Docker 
for Java/Spring Boot-Applications

# Checklist of Tasks:
- [X] Integration of Build-Tools (Maven, Gradle)
- [X] Dockerization of App
  - [X] Enable Containerisation/Build Docker Image App
  - [X] Enable Container-Orchestration with Docker Compose
- [ ] Linting, Code-Checks
- [ ] Define Pipeline in Gitlab for Tests, Code-Quality-Checks, Builds
- [ ] Deployment with Docker-Hub

# How to Run

If the Docker-Image on the system is up-to-date, you can just start the application with: <br>
*docker compose up*

# How to Shut down

You can use docker compose to stop the Application and remove the containers with: <br>
*docker compose down*


# Technologies and Tools used
Java: 17 <br>
Spring Boot: 3.06 <br>
Maven: 3.6.3 <br>
JUnit: 5.0 <br>
Docker: any newer version <br> 